package com.example.dictionaryapplication;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.dictionaryapplication.databinding.FragmentDetailsBinding;
import com.example.dictionaryapplication.model.Word;
import com.example.dictionaryapplication.util.DatabaseAccess;

public class EditFragment extends Fragment {

    private EditText editWord, editDef;
    private Word word;
    private FragmentDetailsBinding binding;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            word = (Word) getArguments().getSerializable("wordToEdit");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        editWord = view.findViewById(R.id.edit_word);
        editDef = view.findViewById(R.id.edit_def);
        editDef.setText(word.getDef());
        editWord.setText(word.getWord());
    }
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_done, menu);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit, container, false);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.mi_done:
                if(editWord.getText().toString().isEmpty()){
                    editWord.requestFocus();
                    editWord.setError("This field can not be empty");
                    return false;
                }
                if(editDef.getText().toString().isEmpty()){
                    editDef.requestFocus();
                    editDef.setError("This field can not be empty");
                    return false;
                }

                DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getActivity());
                databaseAccess.open();
                databaseAccess.editWord(word.getId(),editWord.getText().toString(),editDef.getText().toString());
                databaseAccess.close();
                word.setWord(editWord.getText().toString());
                word.setDef(editDef.getText().toString());
                Bundle bundle = new Bundle();
                bundle.putSerializable("word",word);
                Toast.makeText(getContext(),"Update Word Successfully", Toast.LENGTH_LONG).show();
                Navigation.findNavController(getView()).navigate(R.id.action_editFragment_to_listFragment, bundle);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}