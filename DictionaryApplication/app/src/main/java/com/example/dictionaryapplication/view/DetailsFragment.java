package com.example.dictionaryapplication.view;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.speech.tts.TextToSpeech;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dictionaryapplication.R;
import com.example.dictionaryapplication.databinding.FragmentDetailsBinding;
import com.example.dictionaryapplication.model.MyAdapter;
import com.example.dictionaryapplication.model.Word;
import com.example.dictionaryapplication.util.DatabaseAccess;

import java.util.ArrayList;
import java.util.Locale;

public class DetailsFragment extends Fragment {

    private Word word;
    private WebView wv;
    private TextView tvW;
    private FragmentDetailsBinding binding;
    private ImageButton b1;
    private TextToSpeech t1;
    private int id ;
    private View v;
    private DatabaseAccess databaseAccess = DatabaseAccess.getInstance(getActivity());
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if (getArguments()!=null){
            word = (Word) getArguments().getSerializable("word");
        }

    }
    public void onPause(){
        if(t1 !=null){
            t1.stop();
            t1.shutdown();
        }
        super.onPause();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        t1=new TextToSpeech(getContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.ENGLISH);
                }
            }
        });
        b1 = view.findViewById(R.id.bt_sp);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Toast.makeText(getContext(), word.getWord(),Toast.LENGTH_SHORT).show();
                t1.speak(word.getWord(), TextToSpeech.QUEUE_FLUSH, null);
            }
        });
        v = view;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_details, null,false);
        View viewRoot = binding.getRoot();
        binding.setWordfor(word);
        wv = viewRoot.findViewById(R.id.webview);
        tvW = viewRoot.findViewById(R.id.tv_w);
        wv.loadData(word.getDef(), "text/html", "UTF-8");
        tvW.setText(word.getWord());
        return viewRoot;
        //return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.edit_delete_menu, menu);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.edit:
                Bundle bundle = new Bundle();
                bundle.putSerializable("wordToEdit",word);
                Navigation.findNavController(v).navigate(R.id.action_detailsFragment_to_editFragment, bundle);
                break;
            case R.id.delete:

                Log.d("DEMO","ID= "+word.getId());
                Log.d("DEMO","ID= "+word.getWord());
                databaseAccess.open();
                databaseAccess.deleteWord(word.getId());
                databaseAccess.close();
                Navigation.findNavController(v).navigate(R.id.action_detailsFragment_to_listFragment);
                Toast.makeText(getContext(), "Removed "+word.getWord(),Toast.LENGTH_SHORT).show();
                break;
        }

        return super.onOptionsItemSelected(item);

    }
}